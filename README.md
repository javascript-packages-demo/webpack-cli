# [webpack-cli](https://www.npmjs.com/package/webpack-cli)

Webpack CLI encapsulates all code related to CLI handling. It captures options and sends them to webpack compiler. You can also find functionality for initializing a project and migrating between versions.